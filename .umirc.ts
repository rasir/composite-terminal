import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  history: {
    type: 'hash',
  },
  dynamicImport: {},
  locale: {
    default: 'zh-CN',
    antd: true,
  },
  routes: [
    {
      path: '/',
      component: 'layout',
      routes: [
        {
          path: '/',
          component: 'TabsPage',
        },
        {
          path: '/batch-import',
          component: 'BatchImport',
        },
      ],
    },
  ],
  extraBabelIncludes: ['@tauri-apps/api'],
  fastRefresh: {},
  devServer: {
    port: 3001,
  },
});
