#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

pub mod libs;

use libs::fs_extra::{file_exists, file_read, get_all_pro_dir};
use libs::menu::get_custom_menu;

fn main() {
    let menu = get_custom_menu();
    println!("start");
    fix_path_env::fix().unwrap();
    tauri::Builder::default()
        .menu(menu)
        .invoke_handler(tauri::generate_handler![
            file_exists,
            file_read,
            get_all_pro_dir
        ])
        .on_menu_event(move |event| match event.menu_item_id() {
            "quit" => {
                std::process::exit(0);
            }
            "close" => {
                event.window().close().unwrap();
                // event.window().hide().unwrap();
            }
            "export" => {
                event.window().close().unwrap();
            }
            "debugger" => {
                let window = event.window();
                window.open_devtools();
            }
            "import" => event.window().emit("createWindow", "IMPORT").unwrap(),
            _ => {}
        })
        .on_window_event(|event| match event.event() {
            tauri::WindowEvent::Destroyed => {
                println!("destroyeds");
                event.window().emit("close", 1).unwrap();
            }
            _ => {}
        })
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
