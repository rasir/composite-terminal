use std::path::PathBuf;
use tauri::api::dir::{is_dir, read_dir};
use tauri::api::file::read_string;

#[tauri::command]
pub async fn file_exists(path: PathBuf) -> bool {
    path.exists()
}

#[tauri::command]
pub fn file_read(path: &str) -> String {
    let data = read_string(path).unwrap();
    return data;
}

#[tauri::command]
pub fn get_all_pro_dir(path: &str) -> Vec<String> {
    let file_list = read_dir(path, false).unwrap();
    let mut vec = Vec::new();
    for x in &file_list {
        let xpath = x.path.clone();
        let path_is_dir = is_dir(xpath).unwrap_or(false);
        if path_is_dir {
            let fpath = x.path.clone().join("package.json");
            let is_package_file = fpath.is_file();
            if is_package_file {
                vec.push(x.path.clone().to_str().unwrap().to_string());
            }
        }
    }
    return vec;
}
