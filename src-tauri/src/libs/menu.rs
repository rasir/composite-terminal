use tauri::{CustomMenuItem, Menu, Submenu};

pub fn get_custom_menu() -> Menu {
    /* 标签菜单 */
    let import = CustomMenuItem::new("import".to_string(), "导入目录");
    // let export = CustomMenuItem::new("export".to_string(), "导出会话配置(JSON)");
    /* 文件菜单 */
    let quit = CustomMenuItem::new("quit".to_string(), "退出程序");
    let close = CustomMenuItem::new("close".to_string(), "关闭窗口");
    let debugger = CustomMenuItem::new("debugger".to_string(), "开发模式");
    let file_submenu = Submenu::new(
        "文件",
        Menu::new()
            .add_item(import)
            // .add_item(export)
            .add_item(close)
            .add_item(debugger)
            .add_item(quit),
    );
    return Menu::new()
        // .add_native_item(MenuItem::Copy)
        .add_submenu(file_submenu);
}
