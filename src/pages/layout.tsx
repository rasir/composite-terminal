import { ConfigProvider } from 'antd';

function IndexPage({ children }: any) {
  return (
    <ConfigProvider componentSize="small">
      <div
      // onContextMenu={(e) => e.preventDefault()}
      >
        {children}
      </div>
    </ConfigProvider>
  );
}

export default IndexPage;
