import React from 'react';
import { Button, Tabs, Empty, Tooltip, Modal } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import WorkerItem from '../WorkerItem';

import './index.less';

const { TabPane } = Tabs;

interface TabsContentProps {
  onClear: () => void;
  onTabDelete: (code: string) => void;
  activeKey?: string;
  onActiveChange?: (activeKey: string) => void;
  tabList: TabPaneData[];
  onWorkDataChange: (code: string, workerData: WorkerData) => void;
}

const TabsContent: React.FC<TabsContentProps> = ({
  onTabDelete,
  activeKey,
  onActiveChange,
  tabList,
  onWorkDataChange,
  onClear,
}) => {
  console.log(27, activeKey, tabList);
  const clearTabs = () => {
    Modal.confirm({
      title: '提示',
      content: '是否清空工作空间所有tab？',
      onOk: onClear,
    });
  };
  return tabList.length ? (
    <Tabs
      size="small"
      type="editable-card"
      hideAdd
      onEdit={(code: any) => onTabDelete(code)}
      activeKey={activeKey}
      onChange={onActiveChange}
      tabBarExtraContent={
        <Tooltip title="清空Tab" placement="left">
          <Button size="small" type="primary" onClick={clearTabs}>
            <DeleteOutlined />
          </Button>
        </Tooltip>
      }
    >
      {tabList.map(({ title, code, closeable, data }) => (
        <TabPane key={code} tab={title} closable={closeable}>
          <WorkerItem
            onChange={onWorkDataChange}
            data={data as WorkerData}
            code={code as string}
          />
        </TabPane>
      ))}
    </Tabs>
  ) : (
    <Empty />
  );
};

export default TabsContent;
