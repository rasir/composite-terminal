import React, { useEffect, useRef } from "react";
import { Divider, Skeleton } from "antd";
import { useInViewport } from "ahooks";

interface LoadMoreProps {
  hasMore?: boolean;
  loading?: boolean;
  onLoadMore?: () => void;
}

const LoadMore: React.FC<LoadMoreProps> = ({
  hasMore,
  loading,
  onLoadMore,
}) => {
  const loadMoreRef = useRef<HTMLDivElement>(null);

  const [inViewport] = useInViewport(loadMoreRef);

  useEffect(() => {
    onLoadMore && onLoadMore();
  }, [inViewport, loading, hasMore]);

  return (
    <div className="loadMore" ref={loadMoreRef}>
      {hasMore ? (
        loading ? (
          <Skeleton
            paragraph={{ rows: 1 }}
            active
            style={{ padding: " 5px 20px" }}
          />
        ) : (
          <div style={{ height: 20 }}></div>
        )
      ) : (
        <Divider plain>我是有底线的 🤐</Divider>
      )}
    </div>
  );
};

export default React.memo(LoadMore);
