import ChainPromiseCall from '@rasir/chain-promise-call';

const chianPromiseCall = ChainPromiseCall.getInstance();

const callbackMap = new Map<string, Callback>();

export function logger(logText: string, fileName: string) {
  chianPromiseCall.push({
    run: async () => {
      if (callbackMap.has(fileName)) {
        const callback = callbackMap.get(fileName);
        callback && callback(logText);
      }
    },
  });
}
// 注册回调函数
export async function registerLogger(fileName: string, callback: Callback) {
  chianPromiseCall.push({
    run: async () => {
      callbackMap.set(fileName, callback);
    },
  });
}

export async function unRegisterLogger(fileName: string) {
  callbackMap.delete(fileName);
}
