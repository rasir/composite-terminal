export enum ColorEnum {
  BLACK = '\x1b[30m', // 黑色
  RED = '\x1b[31m', // 红色
  GREEN = '\x1b[32m', // 绿色
  YELLOW = '\x1b[33m', // 黄色
  BLUE = '\x1b[34m', // 蓝色
  MAGENTA = '\x1b[35m', // 紫色
  CYAN = '\x1b[36m', // 深绿
  WHITE = '\x1b[37m', // 白色
  RESET = '\x1b[0m', // 恢复到默认属性
}
