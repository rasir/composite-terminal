import { ColorEnum } from './enums';
import { colorRule } from './types';

export function getTextWithColor(
  text: string | null | undefined,
  rules?: colorRule[],
): string {
  if (!Array.isArray(rules) || rules.length === 0) return text || '';
  if (!text) return '';
  const colorRule = rules.find((rule) => rule.validator(text));
  if (colorRule)
    return `${ColorEnum[colorRule.color]}${text}${ColorEnum.RESET}`;
  return text;
}
