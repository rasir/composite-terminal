import { Terminal } from 'xterm';

export interface TerminalApi {
  clearLog: () => void;
  writeln: (text?: string) => void;
  write: (text?: string) => void;
  getCommand: () => string;
}

export type XTerminal = Terminal & {
  _core: {
    buffer: {
      x: number;
    };
  };
};

type Validator = (text: string) => boolean;

type colorRule = { validator: Validator; color: Color };

export interface XtermTerminalProps {
  height?: number;
  style?: React.CSSProperties;
  onCommand?: (command?: string) => void;
  onClear?: () => void;
  onCopy?: (text: string) => Promise<void>;
  onPaste?: () => Promise<string>;
  disableStdin?: boolean;
  preLog?: string;
  command?: string;
  onLoad?: (term: XTerminal) => void;
  className?: string;
  lineTitle?: string;
  api?: TerminalApi;
  colorRules?: colorRule[]; // 更改一行的文本显示颜色
}

export type Color =
  | 'BLACK'
  | 'RED'
  | 'GREEN'
  | 'YELLOW'
  | 'BLUE'
  | 'MAGENTA'
  | 'CYAN'
  | 'WHITE'
  | 'RESET';
