import React from 'react';
import ReactDOM from 'react-dom';
import { Button } from 'antd';
import { CloseOutlined } from '@ant-design/icons';

import './index.less';

interface ModalProps {
  visible?: boolean;
  title?: string;
  onOk?: () => void;
  onCancel?: () => void;
  children?: React.ReactNode;
  width?: number;
  innerStyle?: React.CSSProperties;
  loading?: boolean;
  className?: string;
}

const show: any = { zIndex: 999, opacity: 1 };
const hide: any = { zIndex: -1, opacity: 0 };
const contShow: any = { minHeight: 120 };
const contHide: any = {};

const Modal: React.FC<ModalProps> = ({
  visible = false,
  title = '标题',
  children,
  onOk,
  onCancel,
  width = 400,
  innerStyle,
  loading,
  className,
}) => {
  return ReactDOM.createPortal(
    <div
      className={`ra-modalContainer ${className || ''}`}
      style={visible ? show : hide}
    >
      <div className="mask" onClick={onCancel}></div>
      <div
        className="innerContent"
        style={
          visible ? { ...contShow, width, ...(innerStyle || {}) } : contHide
        }
      >
        <div className="innerContent-header">
          <div className="innerContent-title">{title}</div>
          <CloseOutlined style={{ cursor: 'pointer' }} onClick={onCancel} />
        </div>
        <div className="innerContent-center">{children}</div>
        <div className="innerContent-footer">
          <Button type="ghost" onClick={onCancel} loading={loading}>
            取消
          </Button>
          <Button type="primary" onClick={onOk} loading={loading}>
            确定
          </Button>
        </div>
      </div>
    </div>,
    document.body,
  );
};

export default React.memo(Modal);
