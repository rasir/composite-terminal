import { Input } from 'antd';
import pasteable from './index';

export default pasteable(Input.Search);
