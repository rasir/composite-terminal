export function getEnumKey(enumObj: any, value: any) {
  return Object.keys(enumObj).find((key) => enumObj[key] === value);
}

export enum TabPaneTypeEnum {
  LIST = 'LIST',
  WORKER = 'WORKER',
}

export enum StorageKeyEnum {
  PROJECT_LIST = 'project_list',
  TAB_PANE_LIST = 'tab_pane_list',
  CATE_LIST = 'cate_list',
}

//"COMMAND" | "TERMINAL"
export enum TerminalTypeEnum {
  COMMAND = 'COMMAND',
  TERMINAL = 'TERMINAL',
}

export enum WinEnums {
  MAIN = 'main',
  IMPORT_WIN = 'importWin',
}
