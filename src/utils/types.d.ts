type TabPaneType = 'LIST' | 'WORKER';

type TerminalType = 'COMMAND' | 'TERMINAL';

interface CommandTerminal {
  type: TerminalType; // 命令名称
  title?: string; // 命令名称
  key: string; // 命令名称
  code?: string; // 项目编码
  command?: string; // 执行命令
  rowNum?: number; // 日志已读行
  workSpace?: string; // 工作空间
  closable?: boolean;
}

type WorkerData = ProjectItem & {
  commandTerminals?: CommandTerminal[]; // code_pid
};

interface TabPaneData {
  title: string;
  closeable?: boolean;
  code?: string;
  type: TabPaneType;
  data?: WorkerData;
}

type SelectOption = {
  label: string;
  value: string;
};

type CommandItem = SelectOption & {
  command: string;
  fixed?: boolean;
  key?: string;
};

interface ProjectItem {
  id?: number;
  cateId?: number; // 分类ID
  index?: number;
  name?: string;
  code?: string;
  description?: string;
  tagName?: string;
  tagCode?: string;
  tagDescription?: string;
  userId?: string;
  workSpace?: string;
  count?: number; // 启动次数
  devUrl?: string; //调试页面
  commands?: CommandItem[];
  createTime?: number;
  updateTime?: number;
  loading?: boolean;
}

interface CataItem {
  id?: number;
  name?: string;
  createTime?: number;
  updateTime?: number;
}

interface PageInfo {
  pageSize: number;
  pageNum: number;
}

type TabPaneListStorage = {
  default: TabPaneData[];
  [key: string]: TabPaneData[];
};

type Callback = (...args: any[]) => void;

interface CateItem {
  name: string;
  id: number;
}
