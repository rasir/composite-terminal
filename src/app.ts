import './index.less';

import { writeText } from '@tauri-apps/api/clipboard';

const registerCopy = async () => {
  await writeText(window.getSelection()?.toString() || '');
};

function copyCmd(e: any) {
  // ctrl+c|command+c
  const { metaKey, ctrlKey, charCode } = e;
  if (charCode !== 99) return;
  if (!(metaKey || ctrlKey)) return;
  registerCopy();
}
window.addEventListener('keypress', copyCmd);
